README.txt

Spreadshirt Module for Drupal
by pepiqueta ([at)} gmail {(dot)] com

Requires Drupal version 4.7.
No contrib modules required.

This module integrates a Spreadshirt shop into a Drupal site.
Read INSTALL.txt for installation instructions.

WARNING: Still in development. Use at your own risk.
         Some features may be missing or incomplete.
         Bugs may appear, even security holes.